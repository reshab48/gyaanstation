
from django.urls import path
from django.contrib.auth.views import LogoutView

from core.views import *

urlpatterns = [

    # Custom admin urls
    path('su/monitor/admin/', comments_admin, name='comments_admin'),

    # Site urls
    path('', index, name='index'),
    path('account/profile/', user_profile, name='user_profile'),
    path('account/anonymous/', set_anonymous, name='set_anonymous'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('truecaller/login', truecaller_login, name='truecaller_login'),
    path('truecaller/auth', truecaller_auth, name='truecaller_auth'),
    path('mentors/', list_mentor, name='list_mentor'),
    path('mentor/<uuid:mentor_id>/', mentor_detail, name='mentor_detail'),
    path('message/respond/<uuid:message_id>/', message_response, name='message_response'),
    path('posts/', list_post, name='list_post'),
    path('posts/<uuid:post_id>/', post_detail, name='post_detail'),
    path('posts/create/', create_post, name='create_post'),
    path('posts/content/add/<uuid:post_id>/<str:ct>/', add_post_content, name='add_post_content'),
    path('posts/rate/', rate_post, name='rate_post'),
    path('mentor/follow/', follow_unfollow_mentor, name='follow_mentor'),
    path('mentor/like/', like_unlike_mentor, name='like_mentor'),
    path('streams/', live_stream, name='live_stream'),
    path('stream/message/respond/<uuid:message_id>/', stream_message_response, name='stream_message_response'),
    path('terms/', terms, name='terms'),
    path('privacy/', privacy, name='privacy'),

]
