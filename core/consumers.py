import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.exceptions import DenyConnection

from core.models import PostComment, Message, Response, StreamMessage, StreamMsgResponse

class AdminConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.room_name = 'admin'
        self.room_group_name = 'admin_group'

        if self.scope['user'].is_staff:
            await self.channel_layer.group_add(
                self.room_group_name,
                self.channel_name
            )

            await self.accept()
        else:
            raise DenyConnection()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        json_data = json.loads(text_data)
        try:
            obj_id = json_data.get('id')
            model_name = json_data.get('model')

            if model_name == PostComment._meta.model_name:
                if json_data.get('action') == 'APP':
                    await self.update_comment(obj_id)
                else:
                    await self.delete_comment(obj_id)
            elif model_name == Message._meta.model_name:
                if json_data.get('action') == 'APP':
                    await self.update_message(obj_id)
                else:
                    await self.delete_message(obj_id)
            elif model_name == Response._meta.model_name:
                if json_data.get('action') == 'APP':
                    await self.update_response(obj_id)
                else:
                    await self.delete_response(obj_id)
            elif model_name == StreamMessage._meta.model_name:
                if json_data.get('action') == 'APP':
                    await self.update_stream_message(obj_id)
                else:
                    await self.delete_stream_message(obj_id)
            elif model_name == StreamMsgResponse._meta.model_name:
                if json_data.get('action') == 'APP':
                    await self.update_stream_msg_response(obj_id)
                else:
                    await self.delete_stream_msg_response(obj_id)

            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type' : 'admin_send',
                    'data' : {
                        'action' : 'UPD',
                        'id' : obj_id,
                        'success' : True
                    }
                }
            )
        except Exception as e:
            await self.channel_layer.group_send(
                self.room_group_name,
                {   
                    'type' : 'admin_send',
                    'data' : {
                        'action' : 'UPD',
                        'id' : obj_id,
                        'success' : False,
                        'message' : str(e)
                    }
                }
            )

    async def admin_send(self, event):
        await self.send(text_data=json.dumps(event['data']))

    @database_sync_to_async
    def update_comment(self, comment_id):
        c = PostComment.objects.get(id=comment_id)
        if not c.approved:
            c.approved = True
            c.save()

    @database_sync_to_async
    def delete_comment(self, comment_id):
        c = PostComment.objects.get(id=comment_id)
        c.delete()

    @database_sync_to_async
    def update_message(self, message_id):
        m = Message.objects.get(id=message_id)
        if not m.approved:
            m.approved = True
            m.save()

    @database_sync_to_async
    def delete_message(self, message_id):
        m = Message.objects.get(id=message_id)
        m.delete()

    @database_sync_to_async
    def update_response(self, response_id):
        r = Response.objects.get(id=response_id)
        if not r.approved:
            r.approved = True
            r.save()

    @database_sync_to_async
    def delete_response(self, response_id):
        r = Response.objects.get(id=response_id)
        r.delete()

    @database_sync_to_async
    def update_stream_message(self, message_id):
        m = StreamMessage.objects.get(id=message_id)
        if not m.approved:
            m.approved = True
            m.save()

    @database_sync_to_async
    def delete_stream_message(self, message_id):
        m = StreamMessage.objects.get(id=message_id)
        m.delete()

    @database_sync_to_async
    def update_stream_msg_response(self, response_id):
        r = StreamMsgResponse.objects.get(id=response_id)
        if not r.approved:
            r.approved = True
            r.save()

    @database_sync_to_async
    def delete_stream_msg_response(self, response_id):
        r = StreamMsgResponse.objects.get(id=response_id)
        r.delete()
