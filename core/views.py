import json, requests

from itertools import chain
from operator import attrgetter

from django.shortcuts import render, redirect, get_object_or_404

from django.views.decorators.csrf import (
    csrf_exempt,
    ensure_csrf_cookie
)
from django.views.decorators.http import require_http_methods
from django.contrib.auth.decorators import login_required

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.humanize.templatetags.humanize import intcomma
from django.contrib.contenttypes.models import ContentType

from django.http.response import (
    HttpResponse,
    JsonResponse
)
from django.contrib.auth import (
    login,
    authenticate
)

from django.conf import settings
from django.utils import timezone
from django.contrib import messages
from django.http import Http404

from django.db.models import Count, Avg
from django.db.models.functions import Coalesce

from django.contrib.auth.models import User

from core.models import *
from core.forms import *
from core.decorators import (
    ajax_required,
    mentee_required
)

# Custom Admin views here.

@staff_member_required
@require_http_methods(['GET'])
def comments_admin(request):
    comments = PostComment.objects.filter(approved=False).distinct()
    messages = Message.objects.filter(approved=False).distinct()
    responses =  Response.objects.filter(approved=False).distinct()
    monitor_objs = sorted(chain(comments, messages, responses), key=attrgetter('created'), reverse=True)
    return render(request, 'su/monitor.html', {'objs' : monitor_objs, 'section' : 'su_comments_admin'})

# Create your views here.

@ensure_csrf_cookie
def index(request):
    t_mentors = Mentor.objects.all().prefetch_related('likes').annotate(num_likes=Count('likes')).order_by('-num_likes')[:6]
    t_posts = Post.objects.all().annotate(avg_rating=Coalesce(Avg('ratings__value'), 0.0)).order_by('-avg_rating')[:20]
    return render(request, 'home/index.html', {'mentors' : t_mentors, 'posts' : t_posts, 'section' : 'index'})

@login_required
@require_http_methods(['GET', 'POST'])
def user_profile(request):
    if request.method == 'POST':
        u_form = UserForm(data=request.POST, instance=request.user)
        m_form = MenteeForm(data=request.POST, files=request.FILES, instance=request.user.mentee)
        if u_form.is_valid() and m_form.is_valid():
            user = u_form.save(commit=False)
            user.save()
            mentee = m_form.save()
            messages.success(request, 'Profile created successfully')
            return redirect('core:user_profile')
    else:
        u_form = UserForm(instance=request.user)
        if hasattr(request.user, 'mentee'):
            m_form = MenteeForm(instance=request.user.mentee)
        else:
            m_form = MenteeForm()
    try:
        dp_pic = request.user.mentee.profile_pic.url
    except:
        dp_pic = None
    anonymous = False
    if hasattr(request.user, 'mentee'):
        anonymous = request.user.mentee.anonymous
    return render(request, 'home/account/profile.html', {'pic' : dp_pic, 'u_form' : u_form, 'm_form' : m_form, 'anonymous' : anonymous, 'section' : 'profile'})

@mentee_required
@ajax_required
@require_http_methods(['POST'])
def set_anonymous(request):
    state = request.POST.get('state')
    if state == 'on':
        new_user_name = request.POST.get('username')
        user = request.user
        try:
            user.username = new_user_name
            user.save()
            mentee = user.mentee
            mentee.anonymous = True
            mentee.save()
        except:
            return JsonResponse({'status' : 'failed'})
    else:
        mentee = request.user.mentee
        mentee.anonymous = False
        mentee.save()
    return JsonResponse({'status' : 'success', 'state' : state})

@ajax_required
@require_http_methods(['POST'])
def truecaller_login(request):
    phone_num = '91{}'.format(request.POST.get('phone_number'))
    try:
        truecaller, created = Truecaller.objects.get_or_create(phone_number=phone_num)
    except Exception as e:
        return JsonResponse({'status' : 'Error', 'mesage' : str(e)})
    if created or timezone.now() >= truecaller.expires_at:
        url = 'https://api4.truecaller.com/v1/apps/requests'
        headers = {'content-type' : 'application/json', 'appKey' : settings.TRUECALLER_APP_KEY}
        data = {'phoneNumber' : phone_num}
        resp = requests.post(url, headers=headers, data=json.dumps(data))
        truecaller.expires_at = timezone.now() + timedelta(minutes=5)
        truecaller.save()
        if resp.ok:
            json_data = json.loads(resp.text)
            truecaller.request_id = json_data['requestId']
            truecaller.save()
            return JsonResponse({'status' : 'Waiting'})
    else:
        user = authenticate(request=request, phone_number=phone_num)
        if user is not None:
            login(request, user)
            messages.success(request, 'Login Successful!')
            return JsonResponse({'status' : 'Success'})
    return JsonResponse({'status' : 'Waiting'})

@csrf_exempt
@require_http_methods(['POST'])
def truecaller_auth(request):
    try:
        json_data = json.loads(request.body.decode(encoding='UTF-8'))
        truecaller = Truecaller.objects.get(request_id=json_data['requestId'])
        truecaller.access_token = json_data['accessToken']
        url = json_data['endpoint']
        headers = {'content-type' : 'application/json', 'Authorization' : 'Bearer {}'.format(truecaller.access_token)}
        resp = requests.get(url, headers=headers)
        if resp.ok:
            true_username = '{}{}'.format(resp.json()['name']['first'], resp.json()['name']['last'])
            user, created = User.objects.get_or_create(username=true_username)
            if created:
                user.first_name = resp.json()['name']['first']
                user.last_name = resp.json()['name']['last']
                Mentee.objects.create(user=user, phone_no=truecaller.phone_number)
            user.save()
            truecaller.user = user
            truecaller.profile_data = json.dumps(resp.json())
            truecaller.expires_at = timezone.now() + timedelta(days=90)
        truecaller.save()
        return JsonResponse({'message' : 'Success'})
    except Exception as e:
        return JsonResponse({'message' : 'Invalid request', 'status' : 'Failed'})

@require_http_methods(['GET'])
def list_mentor(request):
    mentors = Mentor.objects.all().select_related('user')
    t_posts = Post.objects.all().annotate(avg_rating=Coalesce(Avg('ratings__value'), 0.0)).order_by('-avg_rating')[:5]
    return render(request, 'mentors/list.html', {'mentors' : mentors, 'posts' : t_posts, 'section' : 'list_mentor'})

@require_http_methods(['GET', 'POST'])
def mentor_detail(request, mentor_id):
    mentor = Mentor.objects.get(id=mentor_id)
    likes = mentor.likes.all().count()
    followers = mentor.following_mentees.all().count()
    if request.method == 'POST':
        if request.user.is_authenticated:
            form = MessageForm(request.POST)
            if form.is_valid():
                message = form.save(commit=False)
                message.user = request.user
                message.mentor = mentor
                message.save()
                messages.success(request, 'Message added successfully. Waiting for verification.')
                return redirect(reverse('core:mentor_detail', args=[mentor.id]))
        else:
            messages.error(request, 'Only Registered Users can post comments on this board. Sign up NOW')
            return redirect(reverse('core:mentor_detail', args=[mentor.id]))
    else:
        resp_form = ResponseForm()
        form = MessageForm()
    m_messages = mentor.messages.all().filter(approved=True).prefetch_related('responses').select_related('user').distinct()
    return render(request, 'mentors/detail.html', {'mentor' : mentor, 'form' : form, 'resp_form' : resp_form, 'm_messages' : m_messages, 'likes' : likes, 'followers' : followers, 'section' : 'mentor_detail'})

@require_http_methods(['POST'])
def message_response(request, message_id):
    if not request.user.is_authenticated:
        messages.error(request, 'Please Sign-in to reply!')
        return redirect('core:index')
    msg = get_object_or_404(Message, id=message_id)
    form = ResponseForm(request.POST)
    if form.is_valid():
        resp = form.save(commit=False)
        resp.message = msg
        resp.user = request.user
        resp.save()
        messages.success(request, 'Reply submitted successfully. Waiting for verification.')
        return redirect(reverse('core:mentor_detail', args=[msg.mentor.id]))
    messages.error(request, 'Error sending reply! Please try again')
    return redirect(reverse('core:mentor_detail', args=[msg.mentor.id]))

@require_http_methods(['GET'])
def list_post(request):
    posts = Post.objects.all().prefetch_related('target', 'comments', 'ratings').select_related('user').annotate(num_comments=Count('comments', distinct=True), avg_rating=Coalesce(Avg('ratings__value'), 0.0))
    category = request.GET.get('category', None)
    order_by = request.GET.get('order_by', None)
    if category:
        posts = posts.filter(category=category)
    if order_by:
        posts = posts.order_by(order_by)
    for post in posts:
        try:
            rating = post.ratings.all().get(user=request.user)
            post.rated = rating.value
        except Exception as e:
            post.rated = None
    text_form = TextForm()
    video_form = VideoForm()
    audio_form = AudioForm()
    link_form = LinkForm()
    return render(request, 'posts/list.html', {'posts' : posts, 'text_form' : text_form, 'video_form' : video_form, 'audio_form' : audio_form, 'link_form' : link_form, 'section' : 'list_post'})

@require_http_methods(['GET', 'POST'])
def post_detail(request, post_id):
    post = Post.objects.filter(id__in=[post_id]).prefetch_related('target', 'comments', 'ratings').select_related('user').annotate(num_comments=Count('comments', distinct=True), avg_rating=Avg('ratings__value'))[0]
    if request.method == 'POST':
        if request.user.is_authenticated:
            form = CommentForm(request.POST)
            if form.is_valid():
                comment = form.save(commit=False)
                comment.user = request.user
                comment.post = post
                comment.save()
                messages.success(request, 'Comment added successfully. Waiting for verification.')
                return redirect(reverse('core:post_detail', args=[post.id]))
        else:
            messages.error(request, 'Please sign-in to comment!')
            return redirect(reverse('core:post_detail', args=[post.id]))
    else:
        form = CommentForm()
    comments = post.comments.all().filter(approved=True)
    rated = None
    if request.user.is_authenticated:
        try:
            r = post.ratings.all().get(user=request.user)
            rated = r.value
        except Exception as e:
            pass
    return render(request, 'posts/detail.html', {'post' : post, 'rated' : rated, 'comments' : comments, 'form' : form, 'section' : 'post_detail'})

@login_required
@ajax_required
@require_http_methods(['POST'])
def create_post(request):
    categories = ['NWS', 'VID', 'ENT', 'MOT', 'OTH']
    title = request.POST.get('title', None)
    category = request.POST.get('category', None)
    description = request.POST.get('description', None)
    if not title or not category or not description:
        return JsonResponse({'status' : 'failed', 'message' : 'Fields cannot be blank!'})
    if title == '' or category not in categories or description == '':
        return JsonResponse({'status' : 'failed', 'message' : 'Invalid or balnk fields!'})
    post = Post.objects.create(user=request.user, title=title, category=category, description=description)
    return JsonResponse({'status' : 'success', 'post_id' : post.id})

@login_required
@require_http_methods(['POST'])
def add_post_content(request, post_id, ct):
    ct_types = ['text', 'audio', 'video', 'link']
    if ct not in ct_types:
        raise Http404
    if ct == 'text':
        form = TextForm(data=request.POST, files=request.FILES)
    elif ct == 'audio':
        form = AudioForm(data=request.POST, files=request.FILES)
    elif ct == 'video':
        form = VideoForm(data=request.POST)
    elif ct == 'link':
        form = LinkForm(data=request.POST)
    post = get_object_or_404(Post, id=post_id)
    if form.is_valid():
        try:
            content = form.save()
            content_type = ContentType.objects.get(app_label='core', model=content._meta.model_name)
            post.target_ct = content_type
            post.target_id = content.id
            post.save()
        except:
            post.delete()
            messages.error(request, 'Error creating post, please try again.')
            return redirect('core:list_post')
        messages.success(request, 'Post created successfully!')
        return redirect('core:list_post')
    post.delete()
    messages.error(request, 'Error creating post, please try again.')
    return redirect('core:list_post')

@ajax_required
@require_http_methods(['POST'])
def rate_post(request):
    if not hasattr(request.user, 'mentee'):
        return JsonResponse({'status' : 'failed', 'message' : 'Please sign-n to rate the post!'})
    try:
        post = Post.objects.get(id=request.POST['id'])
        rate_val = request.POST['value']
        rating, created = PostRating.objects.get_or_create(user=request.user, post=post)
        rating.value = float(rate_val)
        rating.save()
        avg_r = post.ratings.aggregate(avg_rating=Avg('value'))['avg_rating']
        return JsonResponse({'status' : 'success', 'avg' : round(avg_r, 2)})
    except KeyError:
        return JsonResponse({'status' : 'failed', 'message' : 'Data should have id and value fields.'})
    except Exception as e:
        return JsonResponse({'status' : 'failed', 'message' : str(e)})

@mentee_required
@ajax_required
@require_http_methods(['POST'])
def follow_unfollow_mentor(request):
    try:
        mentor = get_object_or_404(Mentor, id=request.POST['id'])
        mentee = request.user.mentee
        if not mentor in mentee.following.all():
            mentee.following.add(mentor)
        else:
            mentee.following.remove(mentor)
        num_followers = mentor.following_mentees.all().count()
        return JsonResponse({'status' : 'success', 'followers' : intcomma(num_followers) if len(str(num_followers)) < 4 else '{}K'.format(round(num_followers/1000, 1))})
    except KeyError:
        return JsonResponse({'status' : 'failed', 'message' : 'data should have id field'})
    except Exception as e:
        return JsonResponse({'status' : 'failed', 'message' : str(e)})

@mentee_required
@ajax_required
@require_http_methods(['POST'])
def like_unlike_mentor(request):
    try:
        mentor = get_object_or_404(Mentor, id=request.POST['id'])
        user = request.user
        if not user in mentor.likes.all():
            mentor.likes.add(user)
        else:
            mentor.likes.remove(user)
        num_likes = mentor.likes.all().count()
        return JsonResponse({'status' : 'success', 'likes' : intcomma(num_likes) if len(str(num_likes)) < 4 else '{}K'.format(round(num_likes/1000, 1))})
    except KeyError:
        return JsonResponse({'status' : 'failed', 'message' : 'data should have id field'})
    except Exception as e:
        return JsonResponse({'status' : 'failed', 'message' : str(e)})

@require_http_methods(['GET', 'POST'])
def live_stream(request):
    streams = LiveStream.objects.all().select_related('mentor', 'mentor__user')
    stream = streams[0]
    streams = streams[1:]
    if request.method == 'POST':
        messages.error(request, 'Only Premium Users can post comments on this board. Sign up NOW')
        return redirect('core:live_stream')
        if request.user.is_authenticated:
            form = StreamMessageForm(request.POST)
            if form.is_valid():
                message = form.save(commit=False)
                message.user = request.user
                message.stream = stream
                message.save()
                messages.success(request, 'Message added successfully. Waiting for verification.')
                return redirect('core:live_stream')
        else:
            messages.error(request, 'Only Premium Users can post comments on this board. Sign up NOW')
            return redirect('core:live_stream')
    else:
        form = StreamMessageForm()
        resp_form = StreamMsgResponseForm()
    stream_messages = stream.messages.all().filter(approved=True).prefetch_related('responses').select_related('user').distinct()
    return render(request, 'live_streams/index.html', {'stream' : stream, 'streams' : streams, 'form' : form, 'resp_form' : resp_form, 'stream_messages' : stream_messages, 'section' : 'live_stream'})

@require_http_methods(['POST'])
def stream_message_response(request, message_id):
    if not request.user.is_authenticated:
        messages.error(request, 'Please Sign-in to reply!')
        return redirect('core:index')
    msg = get_object_or_404(StreamMessage, id=message_id)
    form = StreamMsgResponseForm(request.POST)
    if form.is_valid():
        resp = form.save(commit=False)
        resp.stream_message = msg
        resp.user = request.user
        resp.save()
        messages.success(request, 'Reply submitted successfully. Waiting for verification.')
        return redirect('core:live_stream')
    messages.error(request, 'Error sending reply! Please try again')
    return redirect('core:live_stream')

def terms(request):
    return render(request, 'terms.html')

def privacy(request):
    return render(request, 'privacy.html')
