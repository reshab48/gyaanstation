from functools import wraps

from django.http import HttpResponseBadRequest, HttpResponseForbidden
from django.shortcuts import redirect
from django.contrib import messages

def ajax_required(func):
    def _func(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return func(request, *args, **kwargs)
    return _func

def mentee_required(func):
    def _func(request, *args, **kwargs):
        if not hasattr(request.user, 'mentee'):
            return HttpResponseForbidden('Must be a registered mentee')
        return func(request, *args, **kwargs)
    return _func

def uploader_decorator(func):

    @wraps(func)
    def _func(request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.error(request, 'User needs to be logged in.')
            return redirect('core:index')
        return func(request, *args, **kwargs)
    return _func
