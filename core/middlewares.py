from django.shortcuts import redirect, reverse
from django.contrib import messages

class MenteeRegistrationCompleteMiddleware(object):
    """
        Redirects user to add profile info if empty
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.user.is_authenticated:
            if not request.user.is_staff and not hasattr(request.user, 'mentor') and request.path != reverse('core:user_profile'):
                if not request.user.email or not request.user.first_name or not request.user.last_name:
                    messages.error(request, 'Please complete your profile to continue browsing gyaanstation')
                    return redirect('core:user_profile')
                if hasattr(request.user, 'mentee'):
                    if not request.user.mentee.gender or not request.user.mentee.phone_no:
                        messages.error(request, 'Please complete your profile to continue browsing gyaanstation')
                        return redirect('core:user_profile')
        return self.get_response(request)

