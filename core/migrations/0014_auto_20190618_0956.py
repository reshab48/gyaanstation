# Generated by Django 2.2.1 on 2019-06-18 09:56

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_auto_20190617_1331'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='livestream',
            options={'ordering': ('-start_at',)},
        ),
        migrations.RenameField(
            model_name='livestream',
            old_name='created',
            new_name='start_at',
        ),
        migrations.RemoveField(
            model_name='livestream',
            name='is_live',
        ),
        migrations.RemoveField(
            model_name='livestream',
            name='updated',
        ),
        migrations.AddField(
            model_name='livestream',
            name='end_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='livestream',
            name='title',
            field=models.CharField(default='', max_length=128),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='livestream',
            name='mentor',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='live_streams', to='core.Mentor'),
        ),
        migrations.AlterField(
            model_name='truecaller',
            name='expires_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 18, 10, 1, 30, 550917, tzinfo=utc)),
        ),
        migrations.DeleteModel(
            name='StreamTopic',
        ),
    ]
