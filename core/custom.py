from django.contrib.auth.models import User

from core.models import Truecaller

from django.utils import timezone

class TruecallerAuthentication:
    def authenticate(self, request, phone_number=None):
        if phone_number:
            try:
                t_user = Truecaller.objects.get(phone_number=phone_number)
                if t_user != None and t_user.profile_data != None and t_user.profile_data != '' and timezone.now() < t_user.expires_at:
                    return t_user.user
            except Exception as e:
                return None
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None