# Generated by Django 2.2.1 on 2019-06-08 12:38

import datetime
import django.core.validators
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20190608_1220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='postrating',
            name='value',
            field=models.DecimalField(blank=True, decimal_places=1, max_digits=2, null=True, validators=[django.core.validators.MinValueValidator(1), django.core.validators.MaxValueValidator(5)]),
        ),
        migrations.AlterField(
            model_name='truecaller',
            name='expires_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 8, 12, 43, 29, 229917, tzinfo=utc)),
        ),
    ]
