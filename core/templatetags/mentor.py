from django import template

from django.contrib.humanize.templatetags.humanize import intcomma

register = template.Library()

@register.filter
def convert_int(obj):
    if len(str(obj)) > 4:
        return '{}K'.format(round(obj/1000, 1))
    return intcomma(obj)

@register.filter
def is_mentee(obj):
    if hasattr(obj, 'mentee'):
        return True
    return False

@register.filter
def approved_responses(obj):
    try:
        return obj.responses.all().filter(approved=True).distinct()
    except:
        return []
