import json

from django import template

register  =template.Library()

@register.filter
def model_name(obj):
    try:
        return obj._meta.model_name
    except AttributeError:
        return None

@register.filter
def audio_thumbnail(obj):
    return json.loads(obj.data)['thumbnail']

@register.filter
def audio_content(obj):
    json_data = json.loads(obj.data)
    return '<h4>{}</h4><p>{}</p>'.format(json_data['title'], json_data['description'])
