# Generated by Django 2.2.1 on 2019-06-02 11:59

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20190527_1830'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='description',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='post',
            name='thumbnail',
            field=models.ImageField(blank=True, null=True, upload_to='posts/'),
        ),
        migrations.AlterField(
            model_name='truecaller',
            name='expires_at',
            field=models.DateTimeField(default=datetime.datetime(2019, 6, 2, 12, 4, 46, 663102, tzinfo=utc)),
        ),
    ]
