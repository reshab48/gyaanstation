from django import forms

from django.contrib.auth.models import User
from django.core.files import File

from core.models import (
    PostComment,
    Message,
    Response,
    Mentee,
    StreamMessage,
    StreamMsgResponse,
    Post,
    Text,
    Audio,
    Video,
    Link
)

from PIL import Image

class CommentForm(forms.ModelForm):
    class Meta:
        model = PostComment
        fields = ('content',)

    def __init__(self, *args, **kwargs):
        super(CommentForm, self).__init__(*args, **kwargs)
        self.fields['content'].required = True

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('content',)

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)
        self.fields['content'].required = True

class ResponseForm(forms.ModelForm):
    class Meta:
        model = Response
        fields = ('resp_content',)

    def __init__(self, *args, **kwargs):
        super(ResponseForm, self).__init__(*args, **kwargs)
        self.fields['resp_content'].required = True

class StreamMessageForm(forms.ModelForm):
    class Meta:
        model = StreamMessage
        fields = ('content',)

    def __init__(self, *args, **kwargs):
        super(StreamMessageForm, self).__init__(*args, **kwargs)
        self.fields['content'].required = True

class StreamMsgResponseForm(forms.ModelForm):
    class Meta:
        model = StreamMsgResponse
        fields = ('resp_content',)

    def __init__(self, *args, **kwargs):
        super(StreamMsgResponseForm, self).__init__(*args, **kwargs)
        self.fields['resp_content'].required = True

class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

class MenteeForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput(), required=False)
    y = forms.FloatField(widget=forms.HiddenInput(), required=False)
    width = forms.FloatField(widget=forms.HiddenInput(), required=False)
    height = forms.FloatField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Mentee
        fields = ('profile_pic', 'gender', 'phone_no', 'x', 'y', 'width', 'height')

    def save(self, *args, **kwargs):
        mentee = super(MenteeForm, self).save(*args, **kwargs)

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')
        try:
            image = Image.open(mentee.profile_pic)
            cropped_image = image.crop((x, y, w+x, h+y))
            cropped_image.save(mentee.profile_pic.path)
        except:
            pass
        return mentee

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'category', 'description')

class TextForm(forms.ModelForm):
    class Meta:
        model = Text
        fields = ('thumbnail',)

class AudioForm(forms.ModelForm):
    class Meta:
        model = Audio
        fields = ('thumbnail', 'audio_file')

class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ('video_url',)

class LinkForm(forms.ModelForm):
    class Meta:
        model = Link
        fields = ('content_url',)
