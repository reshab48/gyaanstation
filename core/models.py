import uuid, json, requests, httplib2

from datetime import timedelta, datetime
from urllib.parse import urlparse, parse_qs

from bs4 import BeautifulSoup

from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow

from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
from django.conf import settings
from django.dispatch import receiver

from django.core.validators import RegexValidator, MinValueValidator, MaxValueValidator

from django.core.exceptions import ValidationError

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.auth.models import User

from ckeditor_uploader.fields import RichTextUploadingField
from ckeditor.fields import RichTextField

from social_django.models import UserSocialAuth

# Custom user methods
def full_name(obj):
    if hasattr(obj, 'mentee'):
        if obj.mentee.anonymous:
            return obj.username
    return '{} {}'.format(obj.first_name, obj.last_name)

User.add_to_class('full_name', full_name)

# Create your models here.
class BaseUserModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    profile_pic = models.ImageField(upload_to='dps/', blank=True, null=True)

    class Meta:
        abstract = True

class Mentor(BaseUserModel):
    user = models.OneToOneField(User, related_name='mentor', on_delete=models.CASCADE)
    industry = models.CharField(max_length=128)
    designation = models.CharField(max_length=128)
    bio = RichTextField(blank=True, config_name='basic')
    likes = models.ManyToManyField(User, related_name='liked_mentors', through='MentorLike', blank=True)

    class Meta:
        db_table = 'mentor'

    def __str__(self):
        return str(self.user)

    def get_absolute_url(self):
        return reverse('core:mentor_detail', args=[self.id])

class MentorLike(models.Model):
    mentor = models.ForeignKey(Mentor, related_name='mentor_likes', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='mentor_likes', on_delete=models.CASCADE)

    class Meta:
        db_table = 'mentor_like'

class Mentee(BaseUserModel):
    GENDER = (
        ('M', 'Male'),
        ('F', 'Female')
    )

    user = models.OneToOneField(User, related_name='mentee', on_delete=models.CASCADE)
    gender = models.CharField(max_length=1, choices=GENDER, blank=True)
    phone_no = models.CharField(max_length=12, validators=[RegexValidator(regex=r'^\d{10,12}$', message='Enter your 10 digits mobile number.')], blank=True)
    anonymous = models.BooleanField(default=False)
    following = models.ManyToManyField(Mentor, related_name='following_mentees', through='MenteeFollow', blank=True)

    class Meta:
        db_table = 'mentee'

    def __str__(self):
        return str(self.user)

class MenteeFollow(models.Model):
    mentee = models.ForeignKey(Mentee, related_name='follows', on_delete=models.CASCADE)
    mentor = models.ForeignKey(Mentor, related_name='follows', on_delete=models.CASCADE)

    class Meta:
        db_table = 'mentee_follow'

class Assistant(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, related_name='assistant', on_delete=models.CASCADE)

    class Meta:
        db_table = 'assistant'

    def __str__(self):
        return str(self.user)

class Post(models.Model):
    POST_CATEGORIES = (
        ('NWS', 'News'),
        ('VID', 'Videos'),
        ('ENT', 'Entertainment'),
        ('MOT', 'Motivational'),
        ('OTH', 'Others')
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE)
    title = models.CharField(max_length=128)
    category = models.CharField(max_length=3, choices=POST_CATEGORIES)
    description = models.TextField()
    target_ct = models.ForeignKey(ContentType, related_name='target_obj', on_delete=models.CASCADE, limit_choices_to=models.Q(app_label='core', model='text')|models.Q(app_label='core', model='audio')|models.Q(app_label='core', model='video')|models.Q(app_label='core', model='link'), null=True, blank=True)
    target_id = models.UUIDField(db_index=True, null=True, blank=True)
    target = GenericForeignKey('target_ct', 'target_id')
    created = models.DateTimeField(auto_now=True, db_index=True)
    updated = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        db_table = 'post'
        ordering = ('-created', '-updated')

    def __str__(self):
        return self.title

    def get_avatar_url(self):
        if hasattr(self.user, 'mentee') and self.user.mentee.profile_pic:
            return self.user.mentee.profile_pic.url
        if hasattr(self.user, 'mentor') and self.user.mentor.profile_pic:
            return self.user.mentor.profile_pic.url
        return None

    @property
    def get_category(self):
        choices = dict(self.POST_CATEGORIES)
        return choices[self.category]

    def get_absolute_url(self):
        return reverse('core:post_detail', args=[self.id])

class Text(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    thumbnail = models.ImageField(upload_to='thumb/text/', blank=True, null=True)

    class Meta:
        db_table = 'post_text'

    def __str__(self):
        return str(self.id)

class Audio(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    thumbnail = models.ImageField(upload_to='thumb/audio/', blank=True, null=True)
    audio_file = models.FileField(upload_to='audio/')

    class Meta:
        db_table = 'post_audio'

    def __str__(self):
        return str(self.id)

class Video(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    video_url = models.URLField()

    class Meta:
        db_table = 'post_video'

    def __str__(self):
        return str(self.id)

    @property
    def img_url(self):
        vid_id = urlparse(self.video_url).path.split('/')[-1]
        return 'https://img.youtube.com/vi/{}/0.jpg'.format(vid_id)

    def full_clean(self, *args, **kwargs):
        try:
            if not 'youtube' in  self.video_url and not 'youtu.be' in self.video_url:
                raise ValidationError({'video_url' : 'Invalid youtube video url'})
            if not 'embed' in self.video_url and not 'youtu.be' in self.video_url:
                q_dict = parse_qs(urlparse(self.video_url).query)
                embed_url = 'https://www.youtube.com/embed/{}'.format(q_dict['v'][0])
                self.video_url = embed_url
            if 'youtu.be' in self.video_url:
                vid_id = urlparse(self.video_url).path.replace('/', '')
                self.video_url = 'https://www.youtube.com/embed/{}'.format(vid_id)
        except:
            raise ValidationError({'video_url' : 'Invalid youtube video url'})
        super(Video, self).full_clean(*args, **kwargs)

class Link(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    content_url = models.URLField()
    data = models.TextField(blank=True)

    class Meta:
        db_table = 'post_link'

    def __str__(self):
        return str(self.id)

    def full_clean(self, *args, **kwargs):
        resp = requests.get(self.content_url)
        if resp.ok:
            soup = BeautifulSoup(resp.text, 'html.parser')
            json_data = None
            try:
                json_data = {
                    'title' : soup.find('meta', {'name' : 'title'})['content'],
                    'description' : soup.find('meta', {'name' : 'description'})['content'],
                    'thumbnail' : soup.find('meta', {'property' : 'og:image'})['content']
                }
            except:
                try:
                    json_data = {
                        'title' : soup.find('meta', {'property' : 'og:title'})['content'],
                        'description' : soup.find('meta', {'property' : 'og:description'})['content'],
                        'thumbnail' : soup.find('meta', {'property' : 'og:image'})['content']
                    }
                except Exception as e:
                    print(str(e))
                    raise ValidationError({'content_url' : 'Invalid content url'})
            self.data = json.dumps(json_data)
        else:
            raise ValidationError({'content_url' : 'Invalid content url'})
        super(Link, self).full_clean(*args, **kwargs)

class PostRating(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, related_name='ratings', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='ratings', on_delete=models.CASCADE)
    value = models.DecimalField(validators=[MinValueValidator(1), MaxValueValidator(5)], max_digits=2, decimal_places=1, null=True, blank=True)

    class Meta:
        db_table = 'post_rating'
        unique_together = ('user', 'post')

    def __str__(self):
        return '{}-> {}'.format(str(self.user), str(self.value))

class PostComment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    content = RichTextField(config_name='small')
    approved = models.BooleanField(default=False, editable=False)
    created = models.DateTimeField(auto_now=True, db_index=True)
    updated = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        db_table = 'post_comment'
        ordering  = ('-created', '-updated')

    def __str__(self):
        return '{}-> {}'.format(str(self.user), str(self.post))

    def save(self, *args, **kwargs):
        if self._state.adding:
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)('admin_group', {
                'type' : 'admin_send',
                'data' : {
                    'model' : self._meta.model_name,
                    'action' : 'INF',
                    'id' : str(self.id),
                    'content' : self.content,
                    'created' : timezone.now().strftime('%D, %d %M, %Y | %P')
                }
            })
        super(PostComment, self).save(*args, **kwargs)

    def get_avatar_url(self):
        if hasattr(self.user, 'mentee') and self.user.mentee.profile_pic:
            return self.user.mentee.profile_pic.url
        if hasattr(self.user, 'mentor') and self.user.mentor.profile_pic:
            return self.user.mentor.profile_pic.url
        return None

class Message(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    mentor = models.ForeignKey(Mentor, related_name='messages', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='messages', on_delete=models.CASCADE)
    content = RichTextUploadingField(config_name='small')
    approved = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True, db_index=True)
    updated = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        db_table = 'message'
        ordering  = ('-created', '-updated')

    def __str__(self):
        return str(self.content)

    def save(self, *args, **kwargs):
        if self._state.adding:
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)('admin_group', {
                'type' : 'admin_send',
                'data' : {
                    'model' : self._meta.model_name,
                    'action' : 'INF',
                    'id' : str(self.id),
                    'content' : self.content,
                    'created' : timezone.now().strftime('%D, %d %M, %Y | %P')
                }
            })
        super(Message, self).save(*args, **kwargs)

    def get_avatar_url(self):
        if hasattr(self.user, 'mentee') and self.user.mentee.profile_pic:
            return self.user.mentee.profile_pic.url
        if hasattr(self.user, 'mentor') and self.user.mentor.profile_pic:
            return self.user.mentor.profile_pic.url
        return None

class Response(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    message = models.ForeignKey(Message, related_name='responses', on_delete=models.CASCADE)
    user  = models.ForeignKey(User, related_name='responses', on_delete=models.CASCADE)
    resp_content = RichTextField(config_name='small')
    approved = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True, db_index=True)
    updated = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        db_table = 'response'
        ordering  = ('-created', '-updated')

    def __str__(self):
        return str(self.content)

    def save(self, *args, **kwargs):
        if self._state.adding:
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)('admin_group', {
                'type' : 'admin_send',
                'data' : {
                    'model' : self._meta.model_name,
                    'action' : 'INF',
                    'id' : str(self.id),
                    'content' : self.resp_content,
                    'created' : timezone.now().strftime('%D, %d %M, %Y | %P')
                }
            })
        super(Response, self).save(*args, **kwargs)

    def get_avatar_url(self):
        if hasattr(self.user, 'mentee') and self.user.mentee.profile_pic:
            return self.user.mentee.profile_pic.url
        if hasattr(self.user, 'mentor') and self.user.mentor.profile_pic:
            return self.user.mentor.profile_pic.url
        return None

class LiveStream(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    mentor = models.ForeignKey(Mentor, related_name='live_streams', on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=128)
    start_at = models.DateTimeField(db_index=True)
    end_at = models.DateTimeField()
    server_key = models.CharField(max_length=128, blank=True)
    server_url = models.URLField(blank=True)
    embed_url = models.URLField(blank=True)

    class Meta:
        db_table = 'live_stream'
        ordering = ('-start_at',)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self._state.adding:
            flow = flow_from_clientsecrets(filename=settings.GOOGLE_SECRET_FILE, scope='https://www.googleapis.com/auth/youtube', message='')
            storage = Storage(settings.GOOGLE_OAUTH_FILE)
            credentials = storage.get()
            if not credentials or credentials.invalid:
                credentials = run_flow(flow, storage)
            youtube = build('youtube', 'v3', credentials.authorize(httplib2.Http()))
            insert_broadcast_response = youtube.liveBroadcasts().insert(
                part='snippet,status',
                body=dict(
                    snippet=dict(
                        title=self.title,
                        scheduledStartTime=self.start_at.strftime("%Y-%m-%dT%H:%M:%SZ"),
                        scheduledEndTime=self.end_at.strftime("%Y-%m-%dT%H:%M:%SZ")
                    ),
                    status=dict(
                        privacyStatus='public'
                    )
                )
            ).execute()
            insert_stream_response = youtube.liveStreams().insert(
                part='snippet,cdn',
                body=dict(
                    snippet=dict(
                        title=self.title
                    ),
                    cdn=dict(
                        format='1080p',
                        ingestionType='rtmp'
                    )
                )
            ).execute()
            bind_broadcast_response = youtube.liveBroadcasts().bind(
                part="id,contentDetails",
                id=insert_broadcast_response['id'],
                streamId=insert_stream_response['id']
            ).execute()
            self.server_key = insert_stream_response['cdn']['ingestionInfo']['streamName']
            self.server_url = insert_stream_response['cdn']['ingestionInfo']['ingestionAddress']
            self.embed_url =  'https://www.youtube.com/embed/{}'.format(bind_broadcast_response['id'])
        super(LiveStream, self).save(*args, **kwargs)

    @property
    def get_avatar_url(self):
        return self.mentor.profile_pic.url if self.mentor.profile_pic else None

    def get_absolute_url(self):
        return reverse('core:live_stream_detail', args=[self.id])

class StreamMessage(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    stream = models.ForeignKey(LiveStream, related_name='messages', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='stream_messages', on_delete=models.CASCADE)
    content = RichTextUploadingField(config_name='small')
    approved = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True, db_index=True)
    updated = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        db_table = 'stream_message'
        ordering  = ('-created', '-updated')

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if self._state.adding:
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)('admin_group', {
                'type' : 'admin_send',
                'data' : {
                    'model' : self._meta.model_name,
                    'action' : 'INF',
                    'id' : str(self.id),
                    'content' : self.content,
                    'created' : timezone.now().strftime('%D, %d %M, %Y | %P')
                }
            })
        super(StreamMessage, self).save(*args, **kwargs)

    def get_avatar_url(self):
        if hasattr(self.user, 'mentee') and self.user.mentee.profile_pic:
            return self.user.mentee.profile_pic.url
        if hasattr(self.user, 'mentor') and self.user.mentor.profile_pic:
            return self.user.mentor.profile_pic.url
        return None

class StreamMsgResponse(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    stream_message = models.ForeignKey(StreamMessage, related_name='responses', on_delete=models.CASCADE)
    user  = models.ForeignKey(User, related_name='stream_msg_responses', on_delete=models.CASCADE)
    resp_content = RichTextField(config_name='small')
    approved = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now=True, db_index=True)
    updated = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        db_table = 'stream_msg_response'
        ordering  = ('-created', '-updated')

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if self._state.adding:
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)('admin_group', {
                'type' : 'admin_send',
                'data' : {
                    'model' : self._meta.model_name,
                    'action' : 'INF',
                    'id' : str(self.id),
                    'content' : self.resp_content,
                    'created' : timezone.now().strftime('%D, %d %M, %Y | %P')
                }
            })
        super(StreamMsgResponse, self).save(*args, **kwargs)

    def get_avatar_url(self):
        if hasattr(self.user, 'mentee') and self.user.mentee.profile_pic:
            return self.user.mentee.profile_pic.url
        if hasattr(self.user, 'mentor') and self.user.mentor.profile_pic:
            return self.user.mentor.profile_pic.url
        return None

class Payment(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user  = models.ForeignKey(User, related_name='payments', on_delete=models.CASCADE)
    amount = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        db_table = 'payment'
        ordering = ('-created',)

    def __str__(self):
        return '{}-> {}'.format(str(self.user), str(self.amount))

class Truecaller(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, related_name='truecaller', null=True, blank=True, on_delete=models.DO_NOTHING)
    phone_number = models.CharField(max_length=12, validators=[RegexValidator(r'^\d{10}$', message='Enter phone number')], unique=True)
    request_id = models.CharField(max_length=28)
    access_token = models.CharField(max_length=64, blank=True, null=True)
    profile_data = models.TextField(blank=True)
    expires_at = models.DateTimeField(blank=True)

    class Meta:
        db_table = 'truecaller'

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if self._state.adding:
            self.expires_at = timezone.now() + timedelta(minutes=5)
        super(Truecaller, self).save(*args, **kwargs)

@receiver(models.signals.post_save, sender=UserSocialAuth)
def social_auth_changed(sender, instance, created, **kwargs):
    if created:
        user = instance.user
        Mentee.objects.create(user=user)
