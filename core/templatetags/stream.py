from django import template

register = template.Library()

@register.filter
def approved_responses(obj):
    try:
        return obj.responses.all().filter(approved=True).distinct()
    except:
        return []
