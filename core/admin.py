from django.contrib import admin

from core.models import *

# Register your models here.
admin.site.register(Mentor)
admin.site.register(Mentee)
admin.site.register(Assistant)
admin.site.register(Post)
admin.site.register(Text)
admin.site.register(Audio)
admin.site.register(Video)
admin.site.register(Link)
admin.site.register(PostRating)
admin.site.register(PostComment)
admin.site.register(Message)
admin.site.register(Response)
admin.site.register(LiveStream)
admin.site.register(Payment)
admin.site.register(Truecaller)
